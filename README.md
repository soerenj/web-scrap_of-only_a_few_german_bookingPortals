## =>[ test it now here online ](http://www.multiple-ical.de/extract_calendar.php) <= online web-Service until Nov. 2021 available

in english short: **experimental** extract occuption calendar of a few (small) german booking portals. 
More english-text, see below...

 
# Kalendar Export; Daten auslesen, aus drei Buchungs-Portalen 
  

Kann direkt als WebService ausprobiert werden: [online webservice](http://www.multiple-ical.de/extract_calendar.php) (bis Nov.2021 online)



Erstellt eine iCal-Kalender datei, aus drei deutschen Buchungs Portalen:
 - tourist-online.de
 - e-domizil.de (auch .at|.ch|.com|.fr|.es, edom.pl, bellevue-ferienhaus.de vacatello.com, e-domizil-premium.com)
 - ferienhausmarkt.com (auch: ostsee-strandurlaub.net, pensionen-weltweit.de)
 

 Es ist und bleibt: **Experimental**. Es könnte jederzeit nicht mehr funktionieren, oder (eher unerwartet) auch falsche Daten anzeigen. 
 Der Kalender wird aus deren Webseite ausgelesen. Sobald die Portale also ihre Webseite ändern, wird diese Script nicht mehr funktionieren.
 
 
 Unterstützt auch Inserate mit mehreren Wohn-Einheiten/Ojecten/Kalendern (nur bei tourist-online):
  - Jeden Kalender (.z.b. Haus/1-Etage/2-Etage) getrennt ausgeben, oder
  - fügt alle Calendar in einen Datei (jedoch seperate Icals-unterdateien) (subid=-1), oder
  - gibt nur voll-Belegungen aus (mindest ein Kalendar ist frei) (subid=-2)
  
 
 Es kann außerdem: Caching (einstellbar); Clean-Urls (via mod-rewrite)
 
 
 Warum habe ich das geschrieben?: Für diese Portale habe ich keinen Kalender export gefunden, daher habe ich das Script geschrieben.
 Wofür braucht man so-etwas?: Wenn man mit einer anderen Software/Webseite einen Überblick über alle seine Buchungs-Portal haben möchte. Eine Software dafür wäre z.B. [overview_multiple_ical_calendar_bookings](https://gitlab.com/soerenj/overview_multiple_ical_calendar_bookings)
 
 Lizenz: GNU APGL
 
 
 
in english:
 
 # extractData_germanBookingsPortals
extract Calendar (iCal) from **three** germany booking portals

This software is also available online as a webservice: [online webservice http://www.multiple-ical.de/extract_calendar.php](http://www.multiple-ical.de/extract_calendar.php) (online Webservice; until 11-2021 online)

Does it still working? Just try, or
 - (if not archived) check under [Issues](https://gitlab.com/soerenj/web-scrap_of-only_a_few_german_bookingPortals/issues) for comment, if it's this working. Please make an entry, if not.
 - Or look in the Feebeck of the online webservice.

supported portals
 - tourist-online.de
 - e-domizil.de (also at|ch|com|fr|es, edom.pl, bellevue-ferienhaus.de, vacatello.com, e-domizil-premium.com)
 - ferienhausmarkt.com, ostsee-strandurlaub.net, pensionen-weltweit.de
 
 It support multi-Room/multi-Calendar from e-domizil/tourist-online. It can: Caching (configurable); Clean-Urls (via mod-rewrite).
 This software is **experimental**, and could break easy: Just if the portal provider change a little bit on there website. It just extract the information from the website.
