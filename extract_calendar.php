<?php
/*
 * extract (web scrapping)) a occupation calender ( of a few proprieiutary Booking-Portals):
 * - tourist-online.de,
 * - e-domizil.de (same ID: e-domizil.at, e-domizil.ch, e-domizil.com, e-domizil.it, e-domizil.es, e-domizil.fr, edom.pl, vacatello.com, bellevue-ferienhaus.de e-domizil-premium.ch)
 * - ferienhausmarkt.com, ostsee-strandurlaub.net, pensionen-weltweit.de
 *
 * https://gitlab.com/soerenj/web-scrap_of-only_a_few_german_bookingPortals
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation.  Please see LICENSE.txt at the top level of
 * the source code distribution for details.
 */
 
$rewrite=false;//mod rewrite
 /** for $rewrite=true, you need to create a .htacces file
  * (mod_rewrite rule is written for installation in main-directoy, not subfolder)
  * .htaccess rewrite
  * RewriteEngine on
  * RewriteCond %{THE_REQUEST} ^[A-Z]{3,9}\ /extract_calendar\.php/([0-9]*)/(-?[0-9]*)/([0-9]*)/(.*)\ HTTP/
  * RewriteRule ^extract_calendar\.php/([0-9]*)/(-?[0-9]*)/([0-9]*)/(.*)$  /extract_calendar.php?caching=$1&subid=$2&skip_leaving_day=$3&url=$4 [L,NC,QSA]
  **/
//if(isset($_GET['debug']))var_dump($_REQUEST);
$getFeedback=false; //only for public Webservice; user can give feedback ist the result is working. 
$text_note = 'welcome'; //a few html lines (in grew)
$footer_contact='your contact adress, or link';
$meta = ''; //tags for search engine

$cacheMaxAgeMinutes_default=0; //0=disable; count in minutes of caching (max 20h)

//var_dump($_GET);
//cache var
$cacheMaxAgeMinutes=$cacheMaxAgeMinutes_default;
if(isset($_GET['caching']))$cacheMaxAgeMinutes=$_GET['caching'];
if(isset($_GET['onlyurl']))$cacheMaxAgeMinutes=0;//disable
// other Parameters: $_GET['move_day_earlier'] //$_GET['skip_leaving_day']
// other Paramter: ?show_dates_as_text=1&show_dates_as_text_names=CalendarEtage1,CalendarEtage2,CalendarEtage3 (no ical file)

//save feedback
$seperat = ' asdsa1l1sasjKalopST1 ';
$allowed_urls = array('tourist-online.de','e-domizil.de', 'e-domizil.at', 'e-domizil.ch', 'e-domizil.com', 'e-domizil.it', 'e-domizil.es', 'e-domizil.fr', 'edom.pl', 'vacatello.com', 'e-domizil-premium.ch', 'bellevue-ferienhaus.de', 'ferienhausmarkt.com','pensionen-weltweit.de','ostsee-strandurlaub.net');
if($getFeedback && isset($_GET['feedback'])){//unlink('extract_calendar_feedback.txt');
  if( preg_match('#https?://(www.)?([^/.]*\.)?([^/\.]*)\.([^/\.]*)/#',$_GET['url'],$match)!==false && count($match)>2){
      $a = $match[ count($match)-2 ]; $b = $match[ count($match)-1 ];
      $url_u = $a.'.'.$b;
      if( !in_array($url_u,$allowed_urls) ) die('URL not allowed, feedback noch processed');
    }else $url_u = '';  
  $myfile = file_put_contents('extract_calendar_feedback.txt', $url_u.$seperat.(int)$_GET['pos'].$seperat.(int)$_GET['neg'].$seperat.(int)time()."\n".PHP_EOL , FILE_APPEND | LOCK_EX);
  die();
}
if( isset($_GET['url'])) $meta .= ' <meta name="robots" content="noindex" />';


?><?php if( !isset($_GET['url'])){?><html lang=de>
<head>
<title>Export / Extract ical-Kalendar</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php echo $meta; ?>
</head>
<body>
<h1>Kalender auslesen: experimentell </h1>
<section id="app">
für einige, wenige Buchungs-Portale; es könnte jederzeit <em>scheitern</em>, wenn sich deren Internet-Seite ändert.<br><br>
<br>
<br>
<?php

if($getFeedback && file_exists('extract_calendar_feedback.txt') && !isset($_GET['onlyurl'])){
  echo "<div style=\"background:#dddddd;border-radius:8pt;padding:2pt;color:#222222;width:400pt;float:right\">Hat es geklappt? (User Feedback):<br>
  <div style=\"height:150pt;width:400pt;padding-left:5pt; overflow:scroll;opacity:0.5\">";
  $lines = explode("\n",file_get_contents('extract_calendar_feedback.txt'));
  $lines = array_reverse($lines);
  $i=0;
  $out='';
  $total = array();
  foreach($lines as $l){
    $i++;
    if($i>20)continue;
    $e = explode($seperat,$l);
    if(count($e)<3)continue;
    if ( !isset( $total[ strip_tags($e[0]) ]))  $total[ strip_tags($e[0]) ] = array(0,0);
    $a = ''; $b='';
    if($e[1]!=0){ $a='<span style="color:green">ja</span>'; $total[ strip_tags($e[0]) ][0]++;}
    if($e[2]!=0){ $b='<span style="color:red">nein</span>';   $total[ strip_tags($e[0]) ][1]++;}
    $out.= strip_tags($e[0]).' '.$a.' '.$b.' '.date('d.m.y H:i',$e[3]).'<br>';
  }
  foreach($total as $key => $val){
    echo 'gesamt: '.$key.' <span style="color:green">+'.$val[0].'</span> <span style="color:red">-'.$val[1].'</span><br>';
  }
  echo "<hr>".$out;
  echo "</div></div>";
}
?>




<form method="get">
  <input type="text" name="url" placeholder="url" required="required" /> <= Adresse ihres Inserats einfügen<br>
  <span style="color:#555555"><br>
   Beispiele:
   <ul>
    <li>http://tourist-online.de/XXXXX.html</li>
    <li>http://e-domizil.de/ferienwohnung/XXXXXX<br>
      <small>  Funktioniert auch mit: .de .at .ch .com .es .fr edom.pl vacatello.com bellevue-ferienhaus.de</small>
    </li>
    <li>http://ferienhausmarkt.com/XXXXX <br>
      <small> Funktioniert auch mit: pensionen-weltweit.de, ostsee-strandurlaub.net </small>
    </li>
   </ul>
   &nbsp; (XXX=Objekt/Inserat-ID)<br>
   <small>&nbsp; Nur diese Portale funktionieren, sihe Liste unten</small><br>
  </span>
  <br>
  <?php if($cacheMaxAgeMinutes>0)echo "Caching/Zwischenspeicher dauer: ".$cacheMaxAgeMinutes.'Minuten<br>'; ?>
  Abreise-Tag auslassen:  <input type="checkbox" name="skip_leaving_day" value="1" /><br>
  <input type="checkbox" name="onlyurl" value="1" checked/>Zeige nur die Adresse (Export-URL)<br>
  
  <input type="submit" id="submit" value="Erstelle ical" />
</form>
</section>


<style>
@media (min-height: 480pt){     #footer { bottom:2pt;z-index:-2;  }   }

a[href^="http://"]:not([href*="multiple-ical.de/"]), a[href^="https://"]:not([href*="multiple-ical.de/"]) {
        padding-right: 13px;
        background-repeat: no-repeat;
        background-position: right center;
        background-image:  url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gUREgI4sosCoQAAAFJJREFUGNNjZMABJux5+B+Zz8iAByArZkTXWeAiz4jNRBZkSWwmFbjIM07Y8/A/Ez7rYAYUuMgzMhFSBANM2DyBzZ0s2BRgA0wMRAIWbEGBDQAA4p4mcMdHanAAAAAASUVORK5CYII=');
    }
</style>
<br>
<section id="content" role="main">
<p style="color:#555555;" id="footer">
 <span style="color:#666666"><?php echo $text_note ?> </span><br><br>
 <hr>
<h2>Über diesen Dienst</h2>

 <span  id="lang_de" lang="de">
     Unterstützte Buchungsportale sind:<br><span style="color:#444444;padding-left: 20pt;display: inline-block;"><?php echo implode(' , ',$allowed_urls )?></span><br>
   <br>
   Service beschreibung:<br>
   <span style="padding-left: 20pt;display: inline-block;">
   Dieser Service liest den Kalender einiger weniger Buchungsportale aus und gibt eine iCal-Kalendar-Datei zurück; Download / Export des eigenen Kalendar-Inserates als .ics / ical Datei.<br>
   Wichtig: Das Auslesen bleibt <b>experimentell</b> und könnte jederzeit scheitern. Bitte das Ergebnis daher regelmäßig prüfen.
   <details>
       <summary style="cursor:pointer;text-decoration:underline">Mini-Anleitung</summary>
       <img src="extract_howto.png" alt="anleitung für Benutzung dieses services / how to"/>
       <br>Die neue erstellte iCal-Url kann überall benutzt werden. In anderen Buchungs-Portalen als Kalender, auf ihrer Homepage im Kalender-System...<br>
       <br>Profi-Tipp: In der Adresse sehen Sie .../0/0/0/ dies bedeutet<ul>
       <li>erste Nummer, für Minuten des Zwischenspeichern (schneller, aber veraltete Ergebnisse); </li>
       <li>zweite Nummer, für Unter-Kalender, die id (meistens nicht benutzt; nur für Kalender mit Unterkalender, z.b. Ferienhäusern mit Unterwohnungen)</li>
       <li>dritte Nummer, für auslassen des Abreise-Tags: Setzt des AbreiseTag als frei</li></ul>
   </details><br>
   </span>
 </span>
 <br>
<br>
 
  <span id="lang_en" lang="en"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAICAYAAADA+m62AAABFElEQVQY012MPUtCYRhAz/Ncr6BXacjFCCkiEGyQXCqC6INaJWqLIqKlbGhrCRr6A06t/YEcm4SGMCmhICkyGhKyELIh8A55vW9DDdEZD5wj+YMTs+LecZzJsjyXwO0dJTw/weXuHvGbCon3F0pLa+hhvYcrYqw3LojaAOAhzEiTYfPJ9eomuaMHAvmtFGon8dxX7KgDGFDFJIcILUxhVVvkcyPI81jWGAO+yM/ttoZEHKxEHFRQEURAak7K8JffAPNPPxbLxlIhYoNGwrQyi9jjaZz9bcQS2nYIz/PRnbJSDsbRxhvWYD/4PhIOERzowz+vcNYwbJQCaOery7Rb575YpdPpAqD4PLUtzOwk6dMCsY8m33ZsW2OPFkQcAAAAAElFTkSuQmCC" />
  Service description in english:<br>
<span style="padding-left: 20pt;display: inline-block;">  Export occupation calendar from three german booking Website; Exporting a File of type iCal (.ics). The Calendars are readed via web-scraping / crawling from theses portals. It's easly break; it's <b>experimental</b>. Please check your results; Only for the listed Portals, see list on end of page. Currently this Service is only a limited time online/available. <details><summary style="cursor:pointer;text-decoration:underline">How To</summary><img src="extract_howto.png" alt="how to use this service"/><br>Your new generated iCal-Url an be used everywhere. In other Booking-Calenders, on your Homepage-Calendar-System...<br><br>Profi-Tip: In Adress you see .../0/0/0/ this means<ul><li>first number, for minutes of caching (faster, but older results); </li><li>second number, for Sub-Calendar id (mostly not used; only for calendars with sub-calendars)</li><li>third number, for skip leaving day: Set the depature day as Free</li></ul></details></span>
</span><br><br><hr>Other Languages:<br><br>  <span id="lang_pl" lang="pl">
<b>PL:</b> Opis usługi w posih Kalendarz zajętości wywozu z rezerwacji edom.pl Strona; Eksportowanie pliku typu iCal (.ics). Kalendarze są odczytywane przez skrobanie w sieci, przeszukiwanie z tych portali. Łatwo to złamać; to jest eksperymentalne. Sprawdź swoje wyniki; Tylko dla wymienionych portali, zobacz listę na końcu strony. Obecnie ta usługa jest dostępna tylko przez ograniczony czas.
</span><br>  <span id="lang_it" lang="it">
<b>IT:</b>
Descrizione del servizio in italiano Calendario di occupazione export dal sito web di prenotazione e-domizil.it; Esportare un file di tipo iCal (.ics). I calendari sono letti via web-scraping, strisciando da questi portali. È facilmente interrotto; è sperimentale. Si prega di controllare i risultati; Solo per i Portali elencati, vedere l'elenco alla fine della pagina. Attualmente questo servizio è disponibile solo per un periodo limitato online.
</span>  <br><span id="lang_es" lang="es">
<b>ES:</b>
Descripción de servicio en español: calendario de ocupación de exportación de e-domizil.es reserva de Sitio Web; la Exportación de un Archivo de tipo iCal (.ics). Los Calendarios son readed vía el raspado de web, que avanza lentamente de puertas de tesis. Esto es la ruptura de easly; es experimental. Por favor compruebe sus resultados; Sólo para las Puertas puestas en una lista, ver la lista durante el final de la página. Actualmente este Servicio es sólo un tiempo limitado en línea disponible.
</span>  <br><span id="lang_fr" lang="fr">
<b>FR:</b>
Description de service dans le français : l'agenda d'occupation d'exportation d'e-domizil.fr le Site Internet réservant; le fait d'Exporter un Dossier de type iCal (.ics). Les Agendas sont readed via la gratture de web, crawlant des portails de thèses. C'est la pause d'easly; c'est expérimental. Vérifiez s'il vous plaît vos résultats; Seulement pour les Portails énumérés, voir la liste sur la fin de page. Actuellement ce Service est seulement un temps limité en ligne disponible.
</span>  <br><span id="lang_nl" lang="nl">
<b>NL:</b>
Servicebeschrijving in het Nederlands:
Exportbezetting kalender van vacatello.com boeking Website; Een bestand van het type iCal (.ics) exporteren. De kalenders worden gelezen via web-scraping, kruipend vanuit deze portals. Het is gemakkelijk te doorbreken; het is experimenteel. Controleer uw resultaten; Alleen voor de weergegeven portals, zie lijst aan het einde van de pagina. Momenteel is deze dienst slechts een beperkte tijd online beschikbaar.
</span>
</p> <br>
<footer>
 <span style="float:right;display:inline-block">this is free Software (licence AGPL) <a href="https://gitlab.com/soerenj/web-scrap_of-only_a_few_german_bookingPortals" ref="nofollow noref">SourceCode</a> <?php if(isset($downloadlinkSoucreCode_optional))echo $downloadlinkSoucreCode_optional; ?></span>
 <?php echo $footer_contact ?>
</footer>
  
</section>

 <script language="javascript">
 //redirect to german site
var language = window.navigator.userLanguage || window.navigator.language;
if(language.substring(0,2)=='es')document.getElementsByTagName('body')[0].insertBefore( document.getElementById('lang_es'), document.getElementsByTagName('h1')[0] );
if(language.substring(0,2)=='it')document.getElementsByTagName('body')[0].insertBefore( document.getElementById('lang_it'), document.getElementsByTagName('h1')[0] );
if(language.substring(0,2)=='pl')document.getElementsByTagName('body')[0].insertBefore( document.getElementById('lang_pl'), document.getElementsByTagName('h1')[0] );
if(language.substring(0,2)=='fr')document.getElementsByTagName('body')[0].insertBefore( document.getElementById('lang_fr'), document.getElementsByTagName('h1')[0] );
if(language.substring(0,2)=='nl')document.getElementsByTagName('body')[0].insertBefore( document.getElementById('lang_nl'), document.getElementsByTagName('h1')[0] );

  if(language.substring(0,2)!='de'){
    /* old: var newItem = document.createElement("H1");
    var textnode = document.createTextNode("Calendar export: experimentell"); 
    newItem.appendChild(textnode);
    document.getElementsByTagName('body')[0].insertBefore( newItem, document.getElementsByTagName('h1')[0] );*/
    document.getElementsByTagName('body')[0].insertBefore( document.getElementById('lang_en'), document.getElementsByTagName('h1')[0] );
    /*olddocument.getElementsByTagName('h1')[1].style.display='none';*/
    document.querySelector('input[type=submit]').value="create ical";
    document.body.innerHTML = document.body.innerHTML.replace(/Kalender auslesen: experimentell/g, 'Calendar export: experimentell');

    document.body.innerHTML = document.body.innerHTML.replace(/für einige, wenige Buchungs-Portale; es könnte jederzeit <em>scheitern<\/em>, wenn sich deren Internet-Seite ändert./g,
    'only for a few Booking sites; it could break / fail easy, if there is Booking-Portal-Website changed');
    document.body.innerHTML = document.body.innerHTML.replace(/Adresse ihres Inserats einfügen/g, 'Insert here Adresse/URL of your Inserat');
    document.body.innerHTML = document.body.innerHTML.replace(/Beispiele/g, 'Examples');
    document.body.innerHTML = document.body.innerHTML.replace(/Beispiel/g, 'Example');
    document.body.innerHTML = document.body.innerHTML.replace(/Funktioniert auch mit/g, 'Also working with');
    document.body.innerHTML = document.body.innerHTML.replace(/Nur diese Portale funktionieren, sihe Liste unten/g, 'Only for this Portals, see list below');
    document.body.innerHTML = document.body.innerHTML.replace(/Abreise-Tag auslassen/g, 'departure day as free');
    document.body.innerHTML = document.body.innerHTML.replace(/Zeige nur die Adresse \(Export-URL\)/g, 'show only the Adress (Export-URL)');
    document.body.innerHTML = document.body.innerHTML.replace(/Dieser Web-Service ist nur/g, 'This Web-Service is only until ');
    document.body.innerHTML = document.body.innerHTML.replace(/erreichbar/g, 'available');
    
    
    document.body.innerHTML = document.body.innerHTML.replace(/Über diesen Dienst/g, 'About');
    document.body.innerHTML = document.body.innerHTML.replace(/Unterstützte Buchungsportale sind:/g, 'Supported Portal-Websites are:');
    document.body.innerHTML = document.body.innerHTML.replace(/Service beschreibung:/g, 'Service description (in german language):');
    
    
    
    

 }
 </script>
 </body>
 </html>
<?php ; exit; }  ?>
<?php if( $getFeedback && isset($_GET['url']) && isset($_GET['onlyurl']) ){  ?>
  <p style="color:#555555;text-align:center;position:fixed;bottom:20pt;right:20pt;z-index:1; background:#78a5df;border-radius:20pt;padding:10pt;" id="Feedback_eck">
  <script language="javascript">
  function feedback(pos,neg){
    document.getElementById('img_placeholder').src='?feedback=1&pos='+pos+'&neg='+neg+'&url=<?php echo str_replace('\'','',urlencode(strip_tags($_GET['url'])));?>';
    document.getElementById('Feedback_eck').innerHTML='Danke fürs Feedback (eng: thanks)';
  }
  </script>
  <span style="padding:5pt;line-height:20pt;"><small style="text-align:right">für statistik:</small><br>Alles richtig?<br>Ist die Datei korrekt?</span><br><!--url=<?php echo ($_GET['url']);?>-->
  <a href="#" onClick="feedback(1,0)" title="Datei richtig, keine Fehler (file correct, no errors, no failure in extracted data)" style="cursor:pointer;background:green;padding:2pt;text-decoration:none">👍</a><a href="#"  onClick="feedback(0,1)" title="Datei falsch, fehlerhaft, hat nicht geklappt (file defect, incorrect, wrong data)" style="cursor:pointer;background:red;padding:2pt;text-decoration:none;">👎</a><img src="" id="img_placeholder" height="1" width="1" />
  </p>
<?php } ?><?php
$url = trim($_GET['url']);

if(substr($url,0,4)=='hex_')
  $url = pack("H*", substr($url,4) );
  

//precheck url
function detectProprietaryUrl($file){
  if( preg_match('#^https?://(www\.)?tourist-online\.de/#',$file,$match)===1){
    return array('manuell' => array('tourist-online'));
  }
  if( preg_match('#^https?://(www\.)?e-domizil\.(de|at|ch|com|it|fr|es)/#',$file,$match)===1 ||
      preg_match("#^https?://(www\.)?edom\.pl/#",$file,$match) === 1 ||
      preg_match("#^https?://(www\.)?vacatello\.com/#",$file,$match) === 1 ||
      preg_match("#^https?://(www\.)?e-domizil-premium\.ch/#",$file,$match) === 1 ){
    return array('manuell' => array('e-domizil'));
  }
  if( preg_match('#^https?://(www\.)?bellevue-ferienhaus\.de/#',$file,$match)===1){
    return array('manuell' => array('bellevue-ferienhaus'));
  }
  if( preg_match('#^https?://(www\.)?ferienhausmarkt\.com/#',$file,$match)===1 || 
      preg_match('#^https?://(www\.)?pensionen-weltweit\.de/#',$file,$match)===1 ||
      preg_match('#^https?://(www\.)?ostsee-strandurlaub\.net/#',$file,$match)===1 ){
    return array('manuell' => array('ferienhausmarkt'));
  }
  return false;
}
if(substr($url,0,8)!='https://' && substr($url,0,7)!='http://')$url='https://'.$url;
if(detectProprietaryUrl($url)===false) die("this adress ist not allowed / diese adresse ist nicht erlaubt");

  $cache_folder = 'cache_extract';
//--cache(Ical) start--//
if($cacheMaxAgeMinutes>0){

 
  //clean cache
  $last_run_file = $cache_folder.'/cache_cleaner_last_run';
  if( !file_exists($last_run_file) || (time() - filemtime($last_run_file)) > 60*$cacheMaxAgeMinutes ){
    foreach (glob($cache_folder."/*") as $fileW) { 
      $e=explode('____',$fileW);if($e[1]>0)$c_t=(int)$e[1];else $c_t=$cacheMaxAgeMinutes;//indiduell cachetime
      if( (time() - filectime($fileW)) > 60*$c_t) unlink($fileW);
      else if( (time() - filectime($fileW)) > 60*60*20) unlink($fileW); //delete after 20h
    }
    file_put_contents($last_run_file,time());
  }
  
  //get from from cache(Ical)
  if($cacheMaxAgeMinutes!=$cacheMaxAgeMinutes_default)$c_t='____'.$cacheMaxAgeMinutes;else;$c_t='';
  $cache_file   = $cache_folder.'/'.md5($_SERVER['REQUEST_URI']).$c_t;
  if(file_exists($cache_file) && (time() - filemtime($cache_file)) < 60*$cacheMaxAgeMinutes ){
      setTheHeaders();
      header("Last-Modified: " . gmdate("D, d M Y H:i:s",filemtime($cache_file)) . " GMT");
      header("X-Cache: ".(int)$cacheMaxAgeMinutes."minutes cacheing active");
      echo file_get_contents($cache_file);
      exit;
  }
}
//--cache end--//


$fromCache = false; //just for RAW-Cache (just Tourist-online)



if( isset($_GET['subid']))$sub_id=(int)$_GET['subid']; else $sub_id='';
$calendar_s = getFile($url);


if($sub_id>=0){ //single calendar
  $events   = array();
  $events   = convertDatesToEvents($calendar_s);
  if(isset($_GET['show_dates_as_text']))foreach($events as $start=>$end)echo "<p>occupied from $start to $end</p>";
  $ical     = createIcal($events);
}


if($sub_id==-1){ //all Calendars, seperated icals, but same file 
  $ical = '';
  foreach($calendar_s as $i_id => $c ){$i++;
    $countSubCalendar++;
    $events = array();
	$events   = convertDatesToEvents($c); 
	if(isset($_GET['show_dates_as_text'])){
	    echo "<h2>Calendar: $i_id" ;
	    $e=explode($_GET['show_dates_as_text_names']);echo ' - '.strip_tags($e[($i-1)]);
	    echo "</h2>";
	    foreach($events as $start=>$end)echo "<p>occupied from $start to $end</p>";
	}
    $ical    .= createIcal($events)."";//no \n! (otherwise the JS will confused and cant seperate the Calendars)
  }
}

if($sub_id==-2){ //not the first Calendar, and only at least one calendar free
  $cs= array();
  foreach($calendar_s as $i_id => $c ){
    $countSubCalendar++;
    $cs[]   = $c;
  }
  $c = mergeAtLeastOneFree($cs);
  $events   = convertDatesToEvents($c);
  if(isset($_GET['show_dates_as_text']))foreach($events as $start=>$end)echo "<p>occupied from $start to $end</p>";
  $ical     = createIcal($events)."";//no \n! (otherwise the JS will confused and cant seperate the Calendars)
}


function mergeAtLeastOneFree($dates_all){
	
	//count & merge in a single Array
	//e.g. [2019][12][31]=2
	$dates_merged = array();
	foreach($dates_all as $dates){
		$y = key($dates);
		$m = key($dates[$y]);
		$d = key($dates[$y][$m]);

		foreach($dates as $y=>$e_year){
			foreach($e_year as $m=>$e_month){
				foreach($e_month as $d=>$value){
				      if( !isset($dates_merged[$y][$m][$d]) ) $dates_merged[$y][$m][$d] = 0;
                      if($value==0.25)$dates_merged[$y][$m][$d]++;
					  if($value>0.75)$dates_merged[$y][$m][$d]++;
				}
			}
		}
	}
	
	//not full booked
	$dates_single = array();
	$y = key($dates_merged);
	$m = key($dates_merged[$y]);
	$d = key($dates_merged[$y][$m]);
	$lastDayValue = 0;
	$lastDayY = 0;
	$lastDayM = 0;
	$lastDayD = 0;

	foreach($dates_merged as $y=>$e_year){
		foreach($e_year as $m=>$e_month){
			foreach($e_month as $d=>$value){
				if($value==count($dates_all)){
				    if($lastDayValue!=count($dates_all) && $lastDayY!=0 )$dates_single[$lastDayY][$lastDayM][$lastDayD] = 0.75;
				    $dates_single[$y][$m][$d]=1;
				}else{
				    if($lastDayValue==count($dates_all) && $lastDayY!=0 )$dates_single[$lastDayY][$lastDayM][$lastDayD] = 0.25;
				}
				$lastDayValue = $value;
				$lastDayY = $y;
				$lastDayM = $m;
				$lastDayD = $d;
			}
		}
	}
	return $dates_single;
}

function getFile($url){
  global  $fromCache, $cacheMaxAgeMinutes, $cache_folder;
  
  if( isset($_GET['subid']))$sub_id=(int)$_GET['subid']; else $sub_id='';
    
    
  
  if( preg_match("#tourist-online\.de/[^0-9]*([0-9]*)(\.html)?#",$url,$match) === 1 ){
    $id=(int)$match[1];
    if( $id==''){ echo "<p>error in url $url. cant process in manual mode. Check the url.</p>"; exit; }
    //old: does not work anymore exec( " wget https://www.tourist-online.de/object/vacancySlider --post-data=objectNumber=".(int)$id." -O Tmpfile.ics" );
	  //just for single Calendar: exec( " wget https://www.tourist-online.de/object/vacancyArrayJson/".(int)$id." -O Tmpfile.ics " );
	  
    $reUrl = "https://www.tourist-online.de/object/partvacancydata/".(int)$id."";
    
    //get from from cache(raw); only for multi Calendars
    if($cacheMaxAgeMinutes!=$cacheMaxAgeMinutes_default)$c_t='____'.$cacheMaxAgeMinutes;else;$c_t='';
    $cache_file   = $cache_folder.'/'.md5($reUrl).$c_t;
    if( $cacheMaxAgeMinutes>0 && file_exists($cache_file) && (time() - filemtime($cache_file)) < 60*$cacheMaxAgeMinutes ){
        $fromCache = gmdate("D, d M Y H:i:s",filemtime($cache_file)); //Caching Header fehlt, funktioniert os nicht
        exec("cp $cache_file Tmpfile.ics ");
    }else{
	      exec("wget $reUrl -O Tmpfile.ics ");
	      if($cacheMaxAgeMinutes>0) exec("cp Tmpfile.ics $cache_file   ");
	  }
	  
	  $json = json_decode(file_get_contents('Tmpfile.ics')); unlink('Tmpfile.ics');
	  if( isset($_GET['debug']))echo var_dump($json).'<hr>';
    if($sub_id>=0)return extractProprietary_TouristOnline($json,$sub_id);
    $out= array();
	if($sub_id==-1)
	{
	  foreach($json as $i_id => $c ){
		  $out[ $i_id ] = extractProprietary_TouristOnline($json,$i_id);
	  }
	}
	if($sub_id==-2)
	{
	  $count_ignore_first=0;
	  foreach($json as $i_id => $c ){
		  if($count_ignore_first==0){ $count_ignore_first++; continue; }
		  $out[ $i_id ] = extractProprietary_TouristOnline($json,$i_id);
	  }
	
	}
    return $out;
  }
  

  
  // --- E-domizil-Art Start --- //
  $continueWithEdom=false;
  
  if( preg_match("#bellevue-ferienhaus\.de/?#",$url,$match) === 1 ){ //only extract the Object-id
    exec( " wget \"$url\" -O Tmpfile.ics" );
    $vacancySlider = file_get_contents('Tmpfile.ics'); unlink('Tmpfile.ics');
    preg_match('#https?://(www.)?bellevue-ferienhaus.de/[^"]*[&?]{1}onr=([0-9]*)#',$vacancySlider,$match);
    $id=(int)$match[ count($match)-1 ];
    if( $id==''){ echo "<p>error in url $url. cant process in manual mode. Check the url.</p>"; exit; }
    $continueWithEdom = true; //fortfahren, wie immer mit e-domizil
  }
  if( preg_match("#(e-domizil)\.(de|at|ch|com|it|es|fr)/([^0-9])*([0-9]*)/?#",$url,$match) === 1 ||
      preg_match("#(edom)\.(pl)/([^0-9])*([0-9]*)/?#",$url,$match) === 1   ||
      preg_match("#(vacatello)\.(com)/([^0-9])*([0-9]*)/?#",$url,$match) === 1   ||
      preg_match("#(e-domizil-premium)\.(ch)/([^0-9])*([0-9]*)/?#",$url,$match) === 1 ||
      $continueWithEdom!==false ){
    if($continueWithEdom===false)$id=(int)$match[ count($match)-1 ];
    $portal_req_url="www.e-domizil.de"; //(default)
    if( preg_match("#(bellevue-ferienhaus)\.(de)/?#",$url,$match) === 1) $portal_req_url = $match[ 1 ].'.'.$match[ 2 ];
    if( $id==''){ echo "<p>error in url $url. cant process in manual mode. Check the url.</p>"; exit; }
    exec( " wget \"https://".$portal_req_url."/index.cfm?event=objekt.object.verfuegbarkeiten&onr=".(int)$id."&size=xl&preserveBuchungsBean=true\" --post-data=objectNumber=".(int)$id." -O Tmpfile.ics" ); 
    exec( "cp cal_edoz-test.ics Tmpfile.ics" ); 
    $continueWithEdom=true;
  }
  if($continueWithEdom===true){
    $vacancySlider = file_get_contents('Tmpfile.ics'); unlink('Tmpfile.ics');
    if( isset($_GET['debug']))echo $vacancySlider;
    return extractProprietary_eDomizilData($vacancySlider,$sub_id);
  }
  // --- E-domizil-Art Ende --- //
  
  if( preg_match("#(ferienhausmarkt\.com)/([0-9]*)#",$url,$match) === 1 ||
      preg_match("#(pensionen-weltweit\.de)/([0-9]*)#",$url,$match) === 1 ||
      preg_match("#(ostsee-strandurlaub\.net)/([0-9]*)#",$url,$match) === 1 ){
    $portal_req_url=$match[1];
    $id=(int)$match[2];
    if( $id==''){ echo "<p>error in url $url. cant process in manual mode. Check the url.</p>"; exit; }
     //curent year
    $date = new DateTime( 'now');
    $y = $date->format('Y');
    exec( " wget https://www.$portal_req_url/".(int)$id."/bookings/$y -O Tmpfile.ics" );
    $bookings = file_get_contents('Tmpfile.ics'); unlink('Tmpfile.ics');
    $return = extractProprietary_FerienhausMarktData($bookings, $y);
     //next year
    $date = new DateTime( 'now');
    $date->modify('+1year');
    $y = $date->format('Y');
    exec( " wget https://www.$portal_req_url/".(int)$id."/bookings/$y -O Tmpfile.ics" );
    $bookings = file_get_contents('Tmpfile.ics'); unlink('Tmpfile.ics');
    
    $a = extractProprietary_FerienhausMarktData($bookings, $y);
    if(isset($a[$y]))$return[$y] = $a[$y];
    return $return;
  }
  
  
  echo "Can't process this Adress  (german: Kann diese Adresse nicht verarbeiten.)";
  exit;
}






//convert $calendar to events
function convertDatesToEvents($dates){

  // example: [2018][1][1] = 0.75 , [2018][1][2] = 1, [2018][1][3] = 0.25    ===>   [2018-1-1]=>[2018-1-3]
  $curentEvent = '';
  //find first Start EventTime (needed if startEvent ist before 1.1.of this year)
  $y = key($dates);
  $m = key($dates[$y]);
  $d = key($dates[$y][$m]);
  if($d!='')$curentEvent = $y.'-'.$m.'-'.$d;

  foreach($dates as $y=>$e_year){
    foreach($e_year as $m=>$e_month){
        foreach($e_month as $d=>$value){
          if($value==0.75)  $curentEvent = $y.'-'.$m.'-'.$d;
          if($value==0.25)  $events[$curentEvent] = $y.'-'.$m.'-'.$d;
          if($value==2.00) {$curentEvent = $y.'-'.$m.'-'.$d; $events[$curentEvent] = $y.'-'.$m.'-'.$d; }
          if($value==1.00) {$events[$curentEvent] = $y.'-'.$m.'-'.$d; }
        }
    }
  }
  return $events;
}


function createIcal($events){

  //find last End EventTime (needed if startEvent is after 31.12.of this year)
  end($events);
  if( key($events)==current($events)){
      $e = explode('-',current($events));
      if($e[1]==12 && $e[2]==31)$events[ key($events) ] = ($e[0]+1).'-1-1';
  }
  reset($events);

  //convert $events  to ical
  $ical = "BEGIN:VCALENDAR \nVERSION:2.0\nPRODID:-//hacksw/handcal//NONSGML v1.0//EN";
  foreach($events as $start => $end){
   
    $dateStart = new Datetime($start);
    $dateEnd   = new Datetime($end);
    
    //DTSTAMP:" . gmdate('Ymd').'T'. gmdate('His') . "Z
    //DTSTART;VALUE=DATE:".$dateStart->format('Ymd')."T150000Z
    //DTEND;VALUE=DATE:".$dateEnd->format('Ymd')."T110000Z

    if(isset($_GET['skip_leaving_day']) && $_GET['skip_leaving_day']==1)$dateEnd->modify('-1 day');

    $ical.="\nBEGIN:VEVENT
UID:" . md5(uniqid(mt_rand(), true)) . "@extracted-calendar.test
DTSTART;VALUE=DATE:".$dateStart->format('Ymd')."
DTEND;VALUE=DATE:".$dateEnd->format('Ymd')."
SUMMARY:reserved
END:VEVENT";
  }
  $ical.="\nEND:VCALENDAR";
  return $ical;
}

if( isset($_GET['onlyurl']) ){
  if( !isset($events) || count($events)==0) echo "<p>Currently <u>no</u> Entrys in Calender (is this correkt?) ;<br>german: Derzeit <u>keine</u> Einträge im Kalendar. (stimmt das?)</p>";
  echo "Die Kalender iCal-URL: "; if(isset($sub_id) && $sub_id!='')echo ' <span style="color:#777777"> ( UnterKalender: '.$sub_id. ') </span> :';
  $onlyUrlOut = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER['HTTP_HOST']."".str_replace('onlyurl=1','',str_replace('http%3A%2F%2F','',str_replace('https%3A%2F%2F','',$_SERVER['REQUEST_URI'])));
  
  if($rewrite){
    $temp_url = str_replace('http://','',str_replace('https://','',$_GET['url']));
    $base = ''.$_SERVER["REQUEST_SCHEME"]."://".$_SERVER['HTTP_HOST']."".$_SERVER['PHP_SELF'].'/'.(int)$_GET['caching'].'/'.(int)$_GET['subid'].'/'.(int)$_GET['skip_leaving_day'].'/';
    $onlyUrlOut  = $base .$temp_url;
    $onlyUrlOut2 = $base .'hex_'.implode(unpack("H*", $temp_url));
  }

  echo "<br><div style=\"margin:12pt;\"><nobr>".$onlyUrlOut."</nobr></div>";
  echo "oder<br><div style=\"margin:12pt;\"><nobr>".$onlyUrlOut."&.ical</nobr></div>";
  echo "oder<br><div style=\"margin:12pt;\"><nobr>".$onlyUrlOut2.".ical</nobr></div>";
  echo "oder<br><div style=\"margin:12pt;\">&#128197;
      <a href=\"http://www.multiple-ical.de/calender_overview.php?disable%5B1%5D=0&name%5B1%5D=Calendar+A&url%5B1%5D=".urlencode($onlyUrlOut2)."&inserat_link%5B1%5D=&inserat_login%5B1%5D=&headline%5B1%5D=&char_cell%5B1%5D=%40&headline_single_char%5B1%5D=&headline_single_char2%5B1%5D=&headline_single_char3%5B1%5D=&seperaLine%5B1%5D=0&color%5B1%5D=%2335b19b&bg_color%5B1%5D=%23eae6e6&notipp=1\" target=\"_blank\">
      Beispiel ansicht</a> <i> (wird evtl. bei Ihnen anders dargestellt werden)</i> | <a href=\"".$onlyUrlOut."\" target=\"_blank\">Download</a> </div>";

}
else
{
  if(!isset($_GET['show_dates_as_text']))setTheHeaders();
  if($fromCache!==false && $fromCache!=''){  //does not Work?
        header("Last-Modified: " . $fromCache . " GMT"); //does not Work?
        header("X-Cache: ".(int)$cacheMaxAgeMinutes."minutes cacheing active");  //does not Work?
  }
  if(!isset($_GET['show_dates_as_text']))echo $ical;
  if($cacheMaxAgeMinutes>0){ //save cache
      if(!file_exists($cache_folder))mkdir($cache_folder);//echo ''.$cacheMaxAgeMinutes.'_'.$cacheMaxAgeMinutes_default;
      if(!file_exists($cache_folder.'/.htaccess'))file_put_contents($cache_folder.'/.htaccess',"order deny,allow\ndeny from all");//echo ''.$cacheMaxAgeMinutes.'_'.$cacheMaxAgeMinutes_default;
      if($cacheMaxAgeMinutes!=$cacheMaxAgeMinutes_default)$c_t='____'.$cacheMaxAgeMinutes;else $c_t='';
      $skip_add = ''; if(isset($_GET['skip_leaving_day']) && $_GET['skip_leaving_day']==1) $skip_add = 'skip';
      file_put_contents($cache_folder.'/'.md5($_SERVER['REQUEST_URI']).$skip_add.$c_t,$ical);
  }
  exit;
}

function setTheHeaders(){
  //set correct content-type-header
  header('Content-type: text/calendar; charset=utf-8');
  header('Content-Disposition: inline; filename=calendar.ics');
}



//for tourist-online and e-domizil
function extractProprietary_TouristOnline($json,$sub_id=''){
    $calendar = array();
    	foreach($json as $i_id => $c ){
	      $countSubCalendar++;
	    }
	  //if( isset($_GET['debug']))  var_dump($json);
	  //select one of the multiple Calandar
	  if( $countSubCalendar==1 ) $sub_id = 1;
	  if( $countSubCalendar>1 && $sub_id==''){
	    echo("Bitte wählen den unterkalender:");
	    echo ("<form method=\"get\"><select name=\"subid\">");
	    foreach($json as $i_id => $c ){
	      echo "<option value=\"$i_id\" >$i_id";
	      if($i_id==1)echo " (ganzes Haus?)";
	      echo"</option>";
	    }
		echo "<option >-1 (alle in einer Datei; jedoch separiert)</option>";
		echo "<option >-2 (nur vollbelegung)</option>";
	    echo "</select>";
	    echo "<input type=\"hidden\" name=\"url\" value=\"".str_replace('"','',strip_tags($_GET['url']))."\" />";
	    if( isset($_GET['onlyurl']) )echo "<input type=\"hidden\" name=\"onlyurl\" value=\"1\" />";
	    if( isset($_GET['skip_leaving_day']) )echo "<input type=\"hidden\" name=\"skip_leaving_day\" value=\"1\" />";
	    echo "<input type=\"submit\" /></form>";
	    die();
	  }
	  
	  if( $sub_id!='' ){
	    if($countSubCalendar==1) { $json = reset($json); }
	    else if($sub_id>0)  $json = $json->$sub_id;
	  }
	  
	  $y_p=0;$m_p=0;$d_p=0;
	  foreach($json as $dateStr => $line)
	  {
		  $dateArray = explode('-',$dateStr);
		  if(count($dateArray)!=3 || strlen((int)$dateArray[0])!=4 ||
			  ( strlen((int)$dateArray[1])!=2 && strlen((int)$dateArray[1])!=1 ) || 
			  ( strlen((int)$dateArray[2])!=2 && strlen((int)$dateArray[2])!=1 ) ) continue;
		      $y = $dateArray[0];
		      $m = $dateArray[1];
		      $d = $dateArray[2];
		      if($line->available == false) $calender[$y][$m][$d] = 1;
		      if($line->available == true)  $calender[$y][$m][$d] = 0;
		      if($y_p>0){ 
                if( $calender[ $y_p ][ $m_p ][ $d_p ]<=0.25 && $calender[$y][$m][$d]==1 ) $calender[ $y ][ $m ][ $d ]=0.75; //arrival day
		        if( $calender[ $y_p ][ $m_p ][ $d_p ]>0.25  && $calender[$y][$m][$d]==0 && $calender[ $y_p ][ $m_p ][ $d_p ]!=2 ) $calender[ $y ][ $m ][ $d ]=0.25; //leaving day
          }
		  $y_p = $y; $m_p = $m; $d_p = $d;//save past
	  }
	  return $calender;
	  
}

//for tourist-online and e-domizil
function extractProprietary_eDomizilData($vacancySlider,$sub_id=''){
  $calendar = array();
	$calendars = preg_split('#class=..?([^"|\']{0,100})?calendar-center#',$vacancySlider);
	$all_subIds = array();
	
	//multiple Calendar?
	foreach($calendars as $c){ //each Calendar
	 //echo "<hr>New Calendar";
	 preg_match('#(calendar-center)?_([0-9]*)#',$c,$match);
	 if( isset($match[2]) && $match[2]!='' ){
	   $all_subIds[ $match[2] ] = $match[2];
	 }
	}
	
	//select one of the multiple Calandar
	if( count($all_subIds)>1 && $sub_id=='' ){
	  echo("Bitte wählen den unterkalender:");
	  echo ("<form method=\"get\"><input type=\"hidden\" name=\"url\" ><select name=\"subid\">");
	  foreach($all_subIds as $i){
	    echo "<option >$i</option>";
	  }
	  echo "</select>";
	  echo "<input type=\"hidden\" name=\"url\" value=\"".str_replace('"','',strip_tags($_GET['url']))."\" />";
	  if( isset($_GET['onlyurl']) )echo "<input type=\"hidden\" name=\"onlyurl\" value=\"1\" />";
	  echo "<input type=\"submit\" /></form>";
	  die();
	}
	
	//detect lang (currently not used)
	// $lang = 'de';
	// if( preg_match("#(www\.)([^\.]*)\.([a-Z]{2,3})/#",$url,$match) === 1 ){
	//   $l=$match[count($match)-2)];
	//   if($l!='') $lang = $l;
	// }
	
	
	foreach($calendars as $c){ //each Calendar
	 if(isset($_GET['debug']))echo "<hr>New Calendar";
	 preg_match('#(calendar-center)?_([0-9]*)#',$c,$match);
	 if( count($all_subIds)>1 && isset($match[2]) && $match[2]!='' ){
	   $calendar_raw_id = $match[2];
	   if($calendar_raw_id!=$sub_id)continue;
	 }
	 $each_month = preg_split('#class=..?([^"|\']{0,100})?month#',$c); //<li class="month dec-2018">Dez 2018</li>
	 
   $monathArray = array();
   $monathArray[1]=array("Jan","Januar","Enero","janvier","styczeń","Januari","gennaio");
   $monathArray[2]=array("Feb","Februar","Febrero","février","luty","Februari","febbraio");
   $monathArray[3]=array("Mar","Mär","März","Marzo","mars","marzec","Maart"."marzo");
   $monathArray[4]=array("Apr","April","Abril","avril","kwiecień","aprile");
   $monathArray[5]=array("Mai","May","Mai","Mayo","mai","maj","Mei","maggio");
   $monathArray[6]=array("Jun","June","Juni","Junio","juin","czerwiec","giugno");
   $monathArray[7]=array("Jul","July","Juli","Julio","juillet","lipiec","luglio");
   $monathArray[8]=array("Aug","August","Agosto","août","sierpień","Augustus","agosto");
   $monathArray[9]=array("Sep","Sept","September","Septiembre","septembre","wrzesień","settembre");
   $monathArray[10]=array("Okt","Oct","Oktober","Octubre","octobre","październik","ottobre");
   $monathArray[11]=array("Nov","November","Noviembre","novembre","listopad","novembre");
   $monathArray[12]=array("Dez","Dec","Dezember","Diciembre","décembre","grudzień","December","dicembre");
	 
	 
	 foreach($each_month as $month_rawdata){ //each Month
	   preg_match('#>(\D*) ([0-9]{4})<#',$month_rawdata,$match);
	   
	   if(isset($_GET['debug']))echo "<hr>New Month".$match[1].'<br>';
	   if( !isset($match[1]) ) continue;
	   $m='';$i=0;
	   foreach($monathArray as $single_monath_items){$i++;
	     if( in_array($match[1],$single_monath_items)===TRUE ) {
	       $m = $i;
	       if(isset($_GET['debug']))echo "<br>month number found: ".$m."<br>";
	       break;
	     }
	   }
	   if($m=='') continue;
	   $y=$match[2];
	   
	   if(isset($_GET['debug']))echo "<br>Month Number:".$y.'-'.$m.'<br>';
	   $each_day = preg_split('#<li .?([^"|\']{0,100})?data-verf#',$month_rawdata);
	   // <li data-verf="BBB" class="available arrival"><em></em><a href="#">02</a></li>
	   foreach($each_day as $day_rawdata){ //each day
		   if(isset($_GET['debug']))echo "<hr> &nbsp;New day".$day_rawdata."<hr>";
		   preg_match('#class="([^"]*)".{0,200}>[^0-9]*([0-9]{1,2})[^0-9]*</#',$day_rawdata,$match);
		   if( !isset($match[2]) || $match[2]==0 ) continue;
		   $d = $match[2];
		   //var_dump($match);
		   //if($d==25)	   var_dump($match);
		   //echo($match[2]." ");
		   if($d<10)$d = substr($d,-1);
       //if($d==2 && $m==11 && $y==2018) { echo "<br>";var_dump($match); echo $day_rawdata."<br>"; }
		   if(isset($_GET['debug']))echo $d;
		   $date_l = new Datetime("$y-$m-$d"); $date_l->modify('-1 day'); //last Day (-1)
       $y_p=$date_l->format('Y'); $m_p=$date_l->format('n'); $d_p=$date_l->format('j');
		   $classes = explode(' ',$match[1]);
		   if(isset($_GET['debug'])){ echo "<br>";var_dump($classes); }
		   if(in_array('not_available',$classes)){
		     $calendar[$y][$m][$d] = 1;
		     //echo " $y $m $d :".$calendar[$y][$m][$d]."<br>";
         //echo " $y $m $d :".$calendar[$y_p][$m_p][$d_p]."<br>";
         if( !isset($calendar[ $y_p ][ $m_p ][ $d_p ]) )     $calendar[$y][$m][$d] = 0.75; //arrival day
         else if($calendar[ $y_p ][ $m_p ][ $d_p ] == 0.25 ) $calendar[$y][$m][$d] = 2;
       }else{
         if(  isset($calendar[ $y_p ][ $m_p ][ $d_p ]) && $calendar[ $y_p ][ $m_p ][ $d_p ]>=0.75 ) $calendar[ $y ][ $m ][ $d ] = 0.25; //departure day
       }
	   }
	 }
	}
	//var_dump( $calendar );
	return $calendar;
}




function extractProprietary_FerienhausMarktData($vacancySlider, $y){
   $calendar = array();
	 if(!isset($timetables)) $timetables = array();
	 $c = preg_split('#<table#',$vacancySlider);
	 if(count($c)<2){ echo "<p>failed to process ferienhausmarkt</p>";$each_month=array();}
	 else{
    $each_month = preg_split('#<tr#',$c[1]);
	 }
	 foreach($each_month as $month_rawdata){ //each Month
	   $m='';
	   $d=0;
	   $each_day = preg_split('#<td#',$month_rawdata);
	   foreach($each_day as $day_rawdata){ //each day
		   if($m==''){
		       preg_match('#[^>]*?class="month[^"]*"[^>]*>([^<]*)#',$day_rawdata,$match);
		     	 if( !isset($match[1]) ) {
		     	   preg_match('#>([0-9]{2})<#',$day_rawdata,$match); //überschrift Tage
		     	   if( count($match)==2){
		     	     if($d>0 && $match!=$d){ echo "<p>Interner Programm-Fehler: Kann Tages-Angaben in der Überschrift nicht abgleichen</p>";die();}
		     	   }else continue;
		     	 }
		       //echo "Month".$match[1]."<br>";
	         if($match[1]=="Jan")$m=1;
	         else if($match[1]=="Feb")$m=2;
	         else if($match[1]=="Mar")$m=3;
	         else if($match[1]=="Apr")$m=4;
	         else if($match[1]=="Mai" || $match[1]=="May")$m=5;
	         else if($match[1]=="Jun")$m=6;
	         else if($match[1]=="Jul")$m=7;
	         else if($match[1]=="Aug")$m=8;
	         else if($match[1]=="Sep")$m=9;
	         else if($match[1]=="Okt" || $match[1]=="Oct")$m=10;
	         else if($match[1]=="Nov")$m=11;
	         else if($match[1]=="Dez" || $match[1]=="Dec")$m=12;
	         
	         continue;
		           
		   }
		   
		   preg_match('#[^>]*class="([^"]*)">.*</td>#',$day_rawdata,$match);
		   if( !isset($match[1]) ) continue;
		   //echo "day counter".$d." ($day_rawdata)<br>";
		   $classes = explode(' ',$match[1]);

		   if($d==0 && !in_array('arrivaldeparture',$classes) && !in_array('free',$classes) && !in_array('occupied',$classes) && !in_array('arrival',$classes) && !in_array('departure',$classes) && !in_array('occupied',$classes)) continue;
		   $d++;
		   if($m<10)$m2='0'.$m;
		   if($d<10)$d = substr($d,-1);
		   $classes = explode(' ',$match[1]);
		   if(in_array('not_available',$classes) || in_array('occupied',$classes)) $calendar[$y][$m][$d] = 1;
		   if(in_array('arrival',$classes))       $calendar[$y][$m][$d] = 0.75;
		   if(in_array('departure',$classes))     $calendar[$y][$m][$d] = 0.25;
		   if(in_array('departure',$classes) && in_array('arrival',$classes))$calendar[$y][$m][$d] = 2;//??
		   if(in_array('arrivaldeparture',$classes))$calendar[$y][$m][$d] = 2;
	   }
	 }
	 return $calendar;
}

$date = new DateTime( 'now');
$y = $date->format('Y');
$m = $date->format('n');

?>